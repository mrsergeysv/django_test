from django.http import FileResponse
from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
import os

from dashboard.celery import celery_app
from dashboard.models import File
from dashboard.serializers import FileSerializer
from django_test.settings import STORAGE_DIR
from .file_processor import process_file


class FileViewSet(viewsets.ViewSet):
    serializer = FileSerializer
    queryset = File.objects.all()

    def get_user_id(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', None)
        if token:
            return Token.objects.get(key=token).user.id
        return 0

    def list(self, request):
        user_id = self.get_user_id(request)
        if user_id:
            files = get_list_or_404(self.queryset, user_id=user_id)
            serializer = self.serializer(files, many=True)
            return Response(serializer.data)
        return Response('Unauthorized', status=401)

    def retrieve(self, request, pk):
        user_id = self.get_user_id(request)
        if user_id:
            db_file = get_object_or_404(self.queryset, pk=pk)
            file_path = os.path.join(STORAGE_DIR, str(user_id), db_file.name)
            if os.path.exists(file_path):
                file = open(file_path, 'rb')
                response = FileResponse(file, as_attachment=True)
                response["Access-Control-Expose-Headers"] = 'Content-Disposition'
                return response

    def create(self, request):
        user_id = self.get_user_id(request)
        if user_id:
            uploaded_file = request.data['file']
            user_directory = os.path.join(STORAGE_DIR, str(user_id))
            if not os.path.exists(user_directory):
                os.makedirs(user_directory)

            filename, extension = os.path.splitext(request.data['file'].name)

            if not os.path.exists(os.path.join(user_directory, request.data['file'].name)):
                full_filename = request.data['file'].name
            else:
                copy_i = 1
                while True:
                    full_filename = "{}({}){}".format(filename, copy_i, extension)
                    if not os.path.exists(os.path.join(user_directory, full_filename)):
                        break
                    copy_i += 1

            with open(os.path.join(user_directory, full_filename), 'wb') as f:
                for chunk in request.data['file'].chunks():
                    f.write(chunk)

            file = File.objects.create(
                name=full_filename,
                size=uploaded_file.size,
                user_id=user_id,
                status='P'
            )
            file_task = process_file.delay(file.id)
            return Response(file_task.id)
        return Response('Unauthorized', status=401)


@api_view(['GET'])
def task_status(request, task_id):
    task_result = celery_app.AsyncResult(str(task_id))
    return Response(task_result.ready())
