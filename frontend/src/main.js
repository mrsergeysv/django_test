import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import VueResource from 'vue-resource'
import VueAuthenticate from 'vue-authenticate'
import VueAxios from 'vue-axios'
import axios from 'axios';
import { routes } from './routes';
import BootstrapVue from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlus, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Meta from 'vue-meta';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

library.add(faPlus, faSignOutAlt, faFacebook)

Vue.component('font-awesome-icon', FontAwesomeIcon)

var options = {
  namespace: 'vuejs__'
};
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue)
Vue.use(Meta)

Vue.use(VueAuthenticate, {
  providers: {
    facebook: {
      name: 'facebook',
      url: 'http://localhost:8000/api/login/social/token_user/facebook/',
      authorizationEndpoint: 'https://www.facebook.com/v3.2/dialog/oauth',
      clientId: '293523551338968',
      redirectUri: 'http://localhost:8080/',
      requiredUrlParams: ['display', 'scope'],
      scope: ['email'],
      scopeDelimiter: ',',
      display: 'popup',
      oauthType: '2.0',
      popupOptions: { width: 580, height: 400 }
    },
  }
});
const router = new VueRouter({
  mode: 'history',
  routes
});
new Vue({
  el: '#app',
  router,
  render: h => h(App),

})

axios.defaults.baseURL = 'http://localhost:8000'
