from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.views.generic import RedirectView

from .views import check_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/dashboard/', include('dashboard.urls')),
    path('', RedirectView.as_view(url='/dashboard')),

    path('api/login/', include('rest_social_auth.urls_token')),
    path('auth/', include('rest_framework_social_oauth2.urls')),
    path('api/check/', check_token),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

]
