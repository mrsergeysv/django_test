from django.urls import path, include
from rest_framework import routers

from dashboard import views
from dashboard.views import task_status

router = routers.DefaultRouter()
router.register(r'file', views.FileViewSet, basename='file')

urlpatterns = [
    path('', include(router.urls)),
    path('file/<uuid:task_id>/status/', task_status),
    ]
