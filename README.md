# Installation
## Clone repo
```
git clone git@gitlab.com:mrsergeysv/django_test.git
```
## Django app
```
cd django_test\engine
```
```
python -m virtualenv venv
```
```
source venv/bin/activate
```
```
pip install -r requirements.txt
```
```
python manage.py migrate
```
Change Celery setings at settings.py
```python
BROKER_URL = "amqp://user_name:password@localhost:5672/host_name"
```


## Frontend
From project root
```
cd frontend
```
```
npm install

```


# Run
## Django app
```
cd engine
```
```
gunicorn django_test.wsgi:application --bind localhost:8000 --workers 3
```
```
celery worker -A dashboard --loglevel=debug --concurrency=4
```
## Frontend
```
cd frontend
```
```
npm run dev
```