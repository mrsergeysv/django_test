from django.conf import settings
from django.db import models

from django.utils import timezone


class File(models.Model):

    FILE_STATUSES = (
        ('R', 'Ready'),
        ('P', 'Pending'),
    )

    name = models.CharField(max_length=30)
    upload_time = models.DateTimeField(default=timezone.now, blank=True)
    size = models.IntegerField()
    status = models.CharField(max_length=1, choices=FILE_STATUSES)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
