import axios from 'axios'

export const apiMixin = {
  methods: {
    getFiles: function (cb) {
      setTimeout(() => {
        axios
          .get('/api/dashboard/file/',
            {headers: {
              authorization: localStorage.getItem("vue-authenticate.vueauth_token")
            }})
          .then(response => cb(null, response.data))
          .catch(error => {
            if (error.response.status === 404) {
              cb(new Error('You don\'t have any files yet.'))
            }
            else{
              cb(new Error('Cannot load files.'))
            }
          })
      }, 100)
    }
  },
  uploadFile: async function (file) {
    var formData = new FormData();
    formData.append("file", file);
    var result = false
      await axios
        .post('/api/dashboard/file/', formData, 
        {headers: {
          'Content-Type': 'multipart/form-data',
          authorization: localStorage.getItem("vue-authenticate.vueauth_token")
        }})
        .then(response => {
          result = response.data
        })
        .catch(error => {
            console.log(error)
          })
    return result
  },
  checkFileStatus: async function(taskId) {
    var result = false
    await axios
        .get('/api/dashboard/file/'+taskId+'/status/',
          {headers: {
            authorization: localStorage.getItem("vue-authenticate.vueauth_token")
          }})
        .then(response => {
          result = response.data
        })
        .catch(error => {
          console.log(error)
        })
    return result
  },
  getFile: async function(fileId) {
    var file = null
    await axios
        .get('/api/dashboard/file/'+fileId+'/',
          {headers: {
              authorization: localStorage.getItem("vue-authenticate.vueauth_token"),
          },
          responseType: "arraybuffer"
        })
        .then(response => {
          var filename = response.request.getResponseHeader('Content-Disposition')
          var filenameRegExp = /filename[^;\n=]*=['"](.*?\2|[^;\n]*)"/g
          file = {
            data: new Blob([response.data], {type: response.request.getResponseHeader('Content-Type')}),
            name: filenameRegExp.exec(filename)[1]}
        })
        .catch(error => {
          console.log(error)
        })
    return file
  }

}
