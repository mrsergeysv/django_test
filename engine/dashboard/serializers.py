import math

from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from dashboard.models import File


class FileSerializer(serializers.HyperlinkedModelSerializer):
    status = serializers.CharField(source='get_status_display')
    upload_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    size = SerializerMethodField()

    def get_size(self, obj):
        return math.ceil(obj.size/1000)

    class Meta:
        model = File
        fields = ('id', 'name', 'upload_time', 'size', 'status')
