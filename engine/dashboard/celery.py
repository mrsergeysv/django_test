import os
from celery import Celery

from django_test.settings import BROKER_URL

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_test.settings')
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')


celery_app = Celery('dashboard', backend='amqp', broker=BROKER_URL)
celery_app.config_from_object('django.conf:settings')
celery_app.conf.update(
        CELERY_TASK_SERIALIZER = 'json',
        CELERY_RESULT_SERIALIZER = 'json',
        CELERY_ACCEPT_CONTENT=['json'],
                )

celery_app.autodiscover_tasks()