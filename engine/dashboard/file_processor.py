import time

from dashboard.celery import celery_app
from dashboard.models import File


@celery_app.task(bind=True)
def process_file(self, file_id):
    time.sleep(3)
    file = File.objects.get(pk=file_id)
    file.status = 'R'
    file.save()
    self.update_state(state='PROGRESS')
